﻿using mybestpe.dap.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace mybestpe.baserepo.Entities
{
    public class DbCategoryModel
    {
        [Column(Name = "ID")]
        public long Id { get; set; }

        [Column(Name = "NAME")]
        public string Name { get; set; }

        [Column(Name = "CODE")]
        public string Code { get; set; }

        [Column(Name = "ImageURL")]
        public string ImageURL { get; set; }

        [Column(Name = "IsActive")]
        public bool IsActive { get; set; }
        public string SITE_URL { get; set; }
    }
}
