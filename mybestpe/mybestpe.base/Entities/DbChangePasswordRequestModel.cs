﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mybestpe.baserepo.Entities
{
    public class DbChangePasswordRequestModel
    {
        public long? CompanyId { get; set; }
        public long? BranchId { get; set; }
        public long? ContactId { get; set; }
        public long? LoginUserId { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
