﻿using mybestpe.dap.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace mybestpe.baserepo.Entities
{
    public class DbCountryModel
    {
        [Column(Name = "COUNTRY_ID")]
        public int Id { get; set; }

        [Column(Name = "NAME")]
        public string Name { get; set; }
    }
}
