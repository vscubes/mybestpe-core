﻿using mybestpe.dap.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace mybestpe.baserepo.Entities
{
    public class DbCustomertModel
    {
        [Column(Name = "NAME")]
        public string Name { get; set;}

        [Column(Name = "roleid")]
        public string RoleId { get; set; }

        [Column(Name = "mobileno")]
        public string MobileNo { get; set; }

        [Column(Name = "phoneno")]
        public string PhoneNo { get; set; }

        [Column(Name = "email")]
        public string Email { get; set; }

        [Column(Name = "isactive")]
        public string IsActive { get; set; }

        [Column(Name = "createddate")]
        public string CreatedDate { get; set; }

        [Column(Name = "company_id")]
        public string CompanyId { get; set; }

        [Column(Name = "branch_id")]
        public string BranchId { get; set; }

        [Column(Name = "ImageURL")]
        public string ImageURL { get; set; }

        [Column(Name = "addressline")]
        public string AddressLine { get; set; }

        [Column(Name = "city")]
        public string City { get; set; }

        [Column(Name = "state_id")]
        public string StateId { get; set; }

        [Column(Name = "country_id")]
        public string CountryId { get; set; }

        [Column(Name = "state_name")]
        public string StateName { get; set; }

        [Column(Name = "country_name")]
        public string CountryName { get; set; }

        [Column(Name = "is_default")]
        public string IsDefault { get; set; }

    }
}
