﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mybestpe.baserepo.Entities
{
    public class DbExecutionReport
    {
        public long a { get; set; }
        public string b { get; set; }
        public decimal c { get; set; }
        public DateTime d { get; set; }
        public short e { get; set; }
        public char f { get; set; }
        public bool g { get; set; }
    }
}
