﻿using mybestpe.dap.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace mybestpe.baserepo.Entities
{
    public class DbForgetPasswordModel
    {
        [Column(Name = "Contact_ID")]
        public string ContactId { get; set; }

        [Column(Name = "Password")]
        public string Password { get; set; }
    }
}
