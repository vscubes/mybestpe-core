﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mybestpe.baserepo.Entities
{
    public class DbGetPointEnableRequestModel
    {
        public int ContactId { get; set; }
        public int? CompanyId { get; set; }
        public int? BranchId { get; set; }
        public int? LoginId { get; set; }
    }
}
