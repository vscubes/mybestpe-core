﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mybestpe.baserepo.Entities
{
    public class DbInsertOfferVisitDetailsModel
    {
        public int OfferDetailsId { get; set; }
        public int ContactId { get; set; }
        public string Url { get; set; }
        public long? CompanyId { get; set; }
        public long? BranchId { get; set; }
        public long? LoginUserId { get; set; }
    }
}
