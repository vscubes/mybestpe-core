﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mybestpe.baserepo.Entities
{
    public class DbInsertPointsDetailsModel
    {
        public int ContactId { get; set; }
        public int Points { get; set; }
        public long? CompanyId { get; set; }
        public long? BranchId { get; set; }
        public long? LoginUserId { get; set; }
        public string Notes { get; set; }
    }
}
