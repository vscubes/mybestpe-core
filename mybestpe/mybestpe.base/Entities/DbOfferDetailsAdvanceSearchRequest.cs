﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mybestpe.baserepo.Entities
{
    public class DbOfferDetailsAdvanceSearchRequest
    {
        public string SearchText { get; set; }
        public int CompanyId { get; set; }
        public int BranchId { get; set; }
        public int? Start { get; set; }
        public int? End { get; set; }
        public int? LoginId { get; set; }
    }
}
