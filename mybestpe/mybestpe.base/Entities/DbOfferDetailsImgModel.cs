﻿using mybestpe.dap.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace mybestpe.baserepo.Entities
{
    public class DbOfferDetailsImgModel
    {
        [Column(Name = "id")]
        public long Id { get; set; }

        [Column(Name = "OfferDetailsId")]
        public long OfferDetailsId { get; set; }

        [Column(Name = "Img")]
        public string Img { get; set; }

        [Column(Name = "IsDefault")]
        public bool? IsDefault { get; set; }

        [Column(Name = "IsActive")]
        public bool? IsActive { get; set; }

        public string SITE_URL { get; set; }
    }
}
