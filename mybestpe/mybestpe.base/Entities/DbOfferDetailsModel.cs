﻿using mybestpe.dap.Extensions;
using System;

namespace mybestpe.baserepo.Entities
{
    public class DbOfferDetailsModel
    {
        [Column(Name = "id")]
        public long Id { get; set; }

        [Column(Name = "OfferDetails")]
        public string OfferDetails { get; set; }

        [Column(Name = "Logo")]
        public string Logo { get; set; }

        [Column(Name = "AboutDetails")]
        public string AboutDetails { get; set; }

        [Column(Name = "CoupenCode")]
        public string CoupenCode { get; set; }

        [Column(Name = "RedemptionProcessLink")]
        public string RedemptionProcessLink { get; set; }

        [Column(Name = "RedemptionProcessDetails")]
        public string RedemptionProcessDetails { get; set; }

        [Column(Name = "TermsandConditions")]
        public string TermsandConditions { get; set; }

        [Column(Name = "ValidStartDate")]
        public DateTime? ValidStartDate { get; set; }

        [Column(Name = "ValidEndDate")]
        public DateTime? ValidEndDate { get; set; }

        [Column(Name = "CustomerCareNumber")]
        public string CustomerCareNumber { get; set; }

        [Column(Name = "CustomerCareMail")]
        public string CustomerCareMail { get; set; }

        [Column(Name = "CommissionSlab")]
        public string CommissionSlab { get; set; }

        [Column(Name = "CategoryId")]
        public int? CategoryId { get; set; }

        [Column(Name = "IsActive")]
        public bool? IsActive { get; set; }

        [Column(Name = "CategoryName")]
        public string CategoryName { get; set; }

        [Column(Name = "VendorId")]
        public int? VendorId { get; set; }

        [Column(Name = "DiscountType")]
        public int? DiscountType { get; set; }

        [Column(Name = "DiscountAmount")]
        public decimal? DiscountAmount { get; set; }

        [Column(Name = "DiscountUpto")]
        public decimal? DiscountUpto { get; set; }
        public string SITE_URL { get; set; }
    }
}
