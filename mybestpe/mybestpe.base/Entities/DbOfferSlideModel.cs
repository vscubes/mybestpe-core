﻿using mybestpe.dap.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace mybestpe.baserepo.Entities
{
    public class DbOfferSlideModel
    {
        [Column(Name = "id")]
        public long Id { get; set; }

        [Column(Name = "OfferDetailsId")]
        public long OfferDetailsId { get; set; }

        [Column(Name = "Link")]
        public string Link { get; set; }

        [Column(Name = "Price")]
        public decimal? Price { get; set; }

        [Column(Name = "ValidStartDate")]
        public DateTime? ValidStartDate { get; set; }

        [Column(Name = "ValidEndDate")]
        public DateTime? ValidEndDate { get; set; }

        [Column(Name = "IsActive")]
        public bool? IsActive { get; set; }

        [Column(Name = "OfferDetails")]
        public string OfferDetails { get; set; }

        [Column(Name = "Logo")]
        public string Logo { get; set; }
        public string SITE_URL { get; set; }
    }
}
