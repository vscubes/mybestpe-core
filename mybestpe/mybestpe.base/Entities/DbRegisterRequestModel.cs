﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mybestpe.baserepo.Entities
{
    public class DbRegisterRequestModel
    {
        public string Name { get; set; }
        public string MobileNo { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public long? CountryId { get; set; }
        public long? StateId { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string LogoPath { get; set; }
    }
}
