﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mybestpe.baserepo.Entities
{
    public class DbResponseModel
    {
        public string ID { get; set; }
        public string CustomMessage { get; set; }
        public string CustomErrorState { get; set; }
    }
}
