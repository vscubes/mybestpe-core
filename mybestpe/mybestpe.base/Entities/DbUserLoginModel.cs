﻿using mybestpe.dap.Extensions;

namespace mybestpe.baserepo.Entities
{
    public class DbUserLoginModel
    {
        [Column(Name = "Contact_ID")]
        public long? ContactId { get; set; }

        [Column(Name = "Branch_ID")]
        public long? BranchId { get; set; }

        [Column(Name = "Company_ID")]
        public long? CompanyId { get; set; }

        [Column(Name = "Branch_LogoPath")]
        public string BranchLogoPath { get; set; }

        [Column(Name = "Company_LogoPath")]
        public string CompanyLogoPath { get; set; }

        [Column(Name = "Contact_Name")]
        public string ContactName { get; set; }

        [Column(Name = "Branch_Name")]
        public string BranchName { get; set; }

        [Column(Name = "Company_Name")]
        public string CompanyName { get; set; }

        [Column(Name = "roleid")]
        public long? RoleId { get; set; }

        [Column(Name = "IsAgent")]
        public bool IsAgent { get; set; }

        [Column(Name = "profile_image_path")]
        public string ProfileImagePath { get; set; }

    }
}
