﻿using mybestpe.dap.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace mybestpe.baserepo.Entities
{
    public class DbVendorListingPositionModel
    {
        [Column(Name = "ID")]
        public long Id { get; set; }

        [Column(Name = "VendorId")]
        public long? VendorId { get; set; }

        [Column(Name = "Link")]
        public string Link { get; set; }

        [Column(Name = "Price")]
        public decimal? Price { get; set; }

        [Column(Name = "ValidStartDate")]
        public DateTime? ValidStartDate { get; set; }

        [Column(Name = "ValidEndDate")]
        public DateTime? ValidEndDate { get; set; }

        [Column(Name = "IsActive")]
        public bool? IsActive { get; set; }

        [Column(Name = "VendorName")]
        public string VendorName { get; set; }

        [Column(Name = "Position")]
        public int? Position { get; set; }

        [Column(Name = "ImageURL")]
        public string ImageURL { get; set; }

        public string SITE_URL { get; set; }
    }
}
