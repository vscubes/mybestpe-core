﻿using System;

namespace mybestpe.baserepo.Exceptions
{
    public class RepositoryException : Exception
    {
        public int? ErrorCode { get; }

        public RepositoryException(string message, Exception innerException = null, int? errorCode = null)
            : base($"{message} ErrorCode:{errorCode}", innerException)
        {
            ErrorCode = errorCode;
        }
    }
}
