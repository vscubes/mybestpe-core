﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace mybestpe.baserepo.Extensions
{
    public static class EnumHelper
    {
        public static string GetDescription(this Enum value)
        {
            var enumMember = value.GetType().GetMember(value.ToString()).FirstOrDefault();
            return !(enumMember?.GetCustomAttribute(typeof(DescriptionAttribute)) is DescriptionAttribute descriptionAttribute)
                    ? value.ToString()
                    : descriptionAttribute.Description;
        }
    }
}
