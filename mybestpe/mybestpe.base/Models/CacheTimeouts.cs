﻿
namespace mybestpe.baserepo.Models
{
    public class CacheTimeouts
    {
        public int LoginInfoCacheMinutes { get; set; } = 1;
        public int CriticalExpirySeconds { get; set; } = 60;
    }
}
