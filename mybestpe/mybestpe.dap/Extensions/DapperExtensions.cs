﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace mybestpe.dap.Extensions
{
    public static class DapperExtensions
    {
        //---------------------------------------------------------------------
        public static Int32 Count(this DynamicParameters parameters)
        {
            return parameters.ParameterNames.Count();
        }

        //---------------------------------------------------------------------
        public static SqlMapper.ICustomQueryParameter AsTableValuedParameter<T>(this IEnumerable<T> items,
            String typeName = null)
        {
            var table = AsDataTable<T>(items);
            return table.AsTableValuedParameter(typeName);
        }

        //---------------------------------------------------------------------
        public static DataTable AsDataTable<T>(this IEnumerable<T> items)
        {
            var table = new DataTable();

            var type = typeof(T);
            var properties = type.GetProperties().Where(i => i.CanRead).ToArray();
            foreach (var p in properties)
            {
                var attributes = p.GetCustomAttributes(true);

                var columnAttributeName = (attributes
                            .FirstOrDefault(i =>
                                i.GetType() == typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute))
                        as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute)
                    ?.Name;

                if (columnAttributeName == null)
                {
                    columnAttributeName = (attributes
                            .FirstOrDefault(i => i.GetType() == typeof(ColumnAttribute)) as ColumnAttribute)
                        ?.Name;
                }

                var columnName = columnAttributeName ?? p.Name;
                var nullableType = Nullable.GetUnderlyingType(p.PropertyType);
                var propType = nullableType ?? p.PropertyType;

                table.Columns.Add(columnName, propType);
            }

            foreach (var item in items)
            {
                var row = table.NewRow();

                for (var i = 0; i < properties.Length; i++)
                    row[table.Columns[i]] = properties[i].GetValue(item) ?? DBNull.Value;

                table.Rows.Add(row);
            }

            return table;
        }
    }
}
