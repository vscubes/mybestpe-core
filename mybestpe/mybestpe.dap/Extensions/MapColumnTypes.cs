﻿using Dapper;
using System;
using System.Linq;
using System.Reflection;

namespace mybestpe.dap.Extensions
{
    public class MapColumnTypes
    {
        //---------------------------------------------------------------------
        public static void Register(Assembly assembly)
        {
            var mappedTypes = assembly.GetTypes().Where(f => f.GetProperties().Any(p => p.GetCustomAttributes(false).Any(a => a.GetType().Name == "ColumnAttribute")));

            var mapper = typeof(ColumnAttributeTypeMapper<>);
            foreach (var mappedType in mappedTypes)
            {
                var genericType = mapper.MakeGenericType(new[] { mappedType });
                SqlMapper.SetTypeMap(mappedType, Activator.CreateInstance(genericType) as SqlMapper.ITypeMap);
            }
        }
    }
}
