﻿using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace mybestpe.repo.Common
{
    public abstract class BaseRepository
    {
        private readonly string _сonnectionString;

        protected BaseRepository(string connectionString)
        {
            if (connectionString == null)
                Console.WriteLine();
            _сonnectionString = connectionString;
        }

        protected async Task<T> ExecuteAsync<T>(Func<DbConnection, Task<T>> operation)
        {
            return await DbExecuteWrapper.ExecuteAsync(_сonnectionString, operation);
        }

        protected async Task ExecuteAsync(Func<DbConnection, Task> operation)
        {
            await DbExecuteWrapper.ExecuteAsync(_сonnectionString, operation);
        }

        protected T Execute<T>(Func<DbConnection, T> operation)
        {
            return DbExecuteWrapper.Execute<T>(_сonnectionString, operation);
        }

        protected void Execute(Action<DbConnection> operation)
        {
            DbExecuteWrapper.Execute(_сonnectionString, operation);
        }

        protected async Task<DataSet> ExecuteForBolt(string spName, Hashtable htParam)
        {
            return await DbExecuteWrapper.ExecuteForBolt(_сonnectionString, spName, htParam);
        }
    }
}
