﻿using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace mybestpe.repo.Common
{
    public static class DbExecuteWrapper
    {
        public static async Task<T> ExecuteAsync<T>(string connectionString, Func<DbConnection, Task<T>> operation)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                await connection.OpenAsync();
                return await operation(connection);
            }
        }

        public static async Task ExecuteAsync(string connectionString, Func<DbConnection, Task> operation)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                await connection.OpenAsync();
                await operation(connection);
            }
        }

        public static T Execute<T>(string connectionString, Func<DbConnection, T> operation)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                return operation(connection);
            }
        }

        public static void Execute(string connectionString, Action<DbConnection> operation)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                operation(connection);
            }
        }
        public static async Task<DataSet> ExecuteForBolt(string connectionString, string spName, Hashtable htParam)
        {
            SqlCommand cmd;
            SqlDataAdapter da;
            IDictionaryEnumerator iEnum = htParam.GetEnumerator();

            using (var connection = new SqlConnection(connectionString))
            {
                await connection.OpenAsync();
                cmd = new SqlCommand(spName, connection);
                while (iEnum.MoveNext())
                {
                    cmd.Parameters.AddWithValue(iEnum.Key.ToString(), iEnum.Value);
                }
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 600;
                da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                connection.Close();
                //var result = Task.FromResult<DataSet>(ds);
                return ds;
            }
        }
    }
}
