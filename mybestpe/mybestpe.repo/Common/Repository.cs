﻿using mybestpe.baserepo.Exceptions;
using mybestpe.dap.Extensions;
using System;
using System.Data.Common;
using System.Data.SqlClient;
using System.Reflection;
using System.Threading.Tasks;

namespace mybestpe.repo.Common
{
    public abstract class Repository : BaseRepository
    {
        static Repository()
        {
           
        }

        protected Repository(string connectionString) : base(connectionString)
        {
        }

        protected new async Task<T> ExecuteAsync<T>(Func<DbConnection, Task<T>> operation)
        {
            try
            {
                return await base.ExecuteAsync(operation);
            }
            catch (SqlException ex)
            {
                throw new RepositoryException(ex.Message, ex);
            }

        }

        protected new T Execute<T>(Func<DbConnection, T> operation)
        {
            try
            {
                return base.Execute(operation);
            }
            catch (SqlException ex)
            {
                throw new RepositoryException(ex.Message, ex);
            }
        }
    }
}
