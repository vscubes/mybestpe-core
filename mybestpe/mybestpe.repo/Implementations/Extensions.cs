﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace mybestpe.repo.Implementations
{
    public static class Extensions
    {
        public static DataTable ToClaimIdsDataTable(this IEnumerable<int> claimsId)
        {
            var dt = new DataTable();
            dt.Columns.Add("id", typeof(string));

            foreach (var id in claimsId)
                dt.Rows.Add(id);

            return dt;
        }
    }
}
