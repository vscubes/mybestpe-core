﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using mybestpe.dap.Extensions;
using mybestpe.repo.Common;
using mybestpe.repo.Interfaces;
using System.Collections;
using mybestpe.baserepo.Entities;
using System.Reflection;

namespace mybestpe.repo.Implementations
{
    public class MyBestPeRepository : Repository, IMyBestPeRepository
    {
        static MyBestPeRepository()
        {
            var assembly = Assembly.GetAssembly(typeof(DbExecutionReport));
            MapColumnTypes.Register(assembly);
        }
        public MyBestPeRepository(string connectionString) : base(connectionString)
        {

        }


        public async Task<DataSet> Bolt(string spName, Hashtable hashtable)
        {
            return await ExecuteForBolt(spName, hashtable);
        }
        public Task<IEnumerable<DbVendorListingPositionModel>> VendorListingPosition(long? id, long companyId, long branchId, long? loginId,int? start, int? end)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Id", id);
            parameters.Add("@Company_ID", companyId);
            parameters.Add("@Branch_ID", branchId);
            parameters.Add("@Login_id", loginId);
            parameters.Add("@start", start);
            parameters.Add("@end", end);
            return ExecuteAsync(conn => conn.QueryAsync<DbVendorListingPositionModel>(
                sql: "dbo.GET_VendorListingPosition",
                param: parameters,
                commandType: CommandType.StoredProcedure));
        }
        public Task<IEnumerable<DbCategoryModel>> GetCategory(long? id, bool? isActive, long companyId, long branchId, long? loginId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Id", id);
            parameters.Add("@IsActive", isActive);
            parameters.Add("@Company_ID", companyId);
            parameters.Add("@Branch_ID", branchId);
            parameters.Add("@Login_id", loginId);

            return ExecuteAsync(conn => conn.QueryAsync<DbCategoryModel>(
                sql: "dbo.GET_CATEGORY",
                param: parameters,
                commandType: CommandType.StoredProcedure));
        }
        public Task<IEnumerable<DbOfferSlideModel>> GetOfferSlideDetails(long? offerDetailsId, long companyId, long branchId, long? loginId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@OfferDetailsId", offerDetailsId);
            parameters.Add("@Company_ID", companyId);
            parameters.Add("@Branch_ID", branchId);
            parameters.Add("@Login_id", loginId);

            return ExecuteAsync(conn => conn.QueryAsync<DbOfferSlideModel>(
                sql: "dbo.GET_OfferDetailsSlideShow",
                param: parameters,
                commandType: CommandType.StoredProcedure));
        }
        public Task<IEnumerable<DbOfferDetailsModel>> GetOfferDetails(long? id, long companyId, long branchId, long? vendorId, long? categoryId, long? loginId, int? start, int? end)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Id", id);
            parameters.Add("@Company_ID", companyId);
            parameters.Add("@Branch_ID", branchId);
            parameters.Add("@VendorId", vendorId);
            parameters.Add("@CategoryId", categoryId);
            parameters.Add("@Login_id", loginId);
            parameters.Add("@start", start);
            parameters.Add("@end", end);

            return ExecuteAsync(conn => conn.QueryAsync<DbOfferDetailsModel>(
                sql: "dbo.GET_OfferDetails",
                param: parameters,
                commandType: CommandType.StoredProcedure));
        }
        public Task<IEnumerable<DbOfferDetailsImgModel>> GetOfferDetailsImg(long? id, long companyId, long branchId, long? loginId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@OfferDetailsId", id);
            parameters.Add("@Company_ID", companyId);
            parameters.Add("@Branch_ID", branchId);
            parameters.Add("@Login_id", loginId);

            return ExecuteAsync(conn => conn.QueryAsync<DbOfferDetailsImgModel>(
                sql: "dbo.GET_OfferDetailsImg",
                param: parameters,
                commandType: CommandType.StoredProcedure));
        }
        public Task<DbChangePasswordModel> ChangePassword(DbChangePasswordRequestModel request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Company_ID", request.CompanyId);
            parameters.Add("@Branch_ID", request.BranchId);
            parameters.Add("@Contact_ID", request.ContactId);            
            parameters.Add("@Old_Password", request.OldPassword);
            parameters.Add("@New_password", request.NewPassword);
            parameters.Add("@Login_user_ID", request.LoginUserId);
            return ExecuteAsync(conn => conn.QueryFirstOrDefaultAsync<DbChangePasswordModel>(
                sql: "dbo.CHANGE_PASSWORD",
                param: parameters,
                commandType: CommandType.StoredProcedure));
        }
        public Task<DbForgetPasswordModel> ForgetPassword(string email)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@EMAIL", email);
            return ExecuteAsync(conn => conn.QueryFirstOrDefaultAsync<DbForgetPasswordModel>(
                sql: "dbo.FORGET_PASSWORD",
                param: parameters,
                commandType: CommandType.StoredProcedure));
        }
        public Task<DbUserRegisterModel> UserRegistration(DbRegisterRequestModel request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Name", request.Name);
            parameters.Add("@MobileNo", request.MobileNo);
            parameters.Add("@PhoneNo", request.PhoneNo);
            parameters.Add("@Email", request.Email);
            parameters.Add("@Password", request.Password);
            parameters.Add("@Country_id", request.CountryId);
            parameters.Add("@State_id", request.StateId);
            parameters.Add("@City", request.City);
            parameters.Add("@Address", request.Address);
            parameters.Add("@LogoPath", request.LogoPath);
            return ExecuteAsync(conn => conn.QueryFirstOrDefaultAsync<DbUserRegisterModel>(
                sql: "dbo.INSERT_CUSTOMER_DETAILS",
                param: parameters,
                commandType: CommandType.StoredProcedure));
        }
        public async Task<int> UpdateCustomerDetails(DbUpdateCustomerDetailsModel request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@ID", request.Id);
            parameters.Add("@Name", request.Name);
            parameters.Add("@MobileNo", request.MobileNo);
            parameters.Add("@PhoneNo", request.PhoneNo);
            parameters.Add("@Email", request.Email);
            parameters.Add("@Country_id", request.CountryId);
            parameters.Add("@State_id", request.StateId);
            parameters.Add("@City", request.City);
            parameters.Add("@Address", request.Address);
            parameters.Add("@WebsiteUrl", request.LogoPath);
            var res = await ExecuteAsync( conn => conn.QueryFirstOrDefaultAsync<int>(
                sql: "dbo.UPDATE_CUSTOMER_DETAILS",
                param: parameters,
                commandType: CommandType.StoredProcedure));
            return res;
        }
        public Task<DbUserLoginModel> UserLogin(string email, string password)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Email", email);
            parameters.Add("@Password", password);
            return ExecuteAsync(conn => conn.QueryFirstOrDefaultAsync<DbUserLoginModel>(
                sql: "dbo.VALIDATE_LOGIN",
                param: parameters,
                commandType: CommandType.StoredProcedure));
        }
        public Task InsertOfferVisitDetails(DbInsertOfferVisitDetailsModel request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@OfferDetailsId", request.OfferDetailsId);
            parameters.Add("@Contact_Id", request.ContactId);
            parameters.Add("@Url", request.Url);
            parameters.Add("@Company_ID", request.CompanyId);
            parameters.Add("@Branch_ID", request.BranchId);
            parameters.Add("@Login_user_ID", request.LoginUserId);
            return ExecuteAsync(conn => conn.QueryAsync(
                sql: "dbo.INSERT_OFFER_VISIT_DETAILS",
                param: parameters,
                commandType: CommandType.StoredProcedure));
        }
        public Task InsertPointsDetails(DbInsertPointsDetailsModel request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Contact_Id", request.ContactId);
            parameters.Add("@Points", request.Points);
            parameters.Add("@Company_ID", request.CompanyId);
            parameters.Add("@Branch_ID", request.BranchId);
            parameters.Add("@Login_user_ID", request.LoginUserId);
            parameters.Add("@Notes", request.Notes);
            return ExecuteAsync(conn => conn.QueryAsync(
                sql: "dbo.INSERT_POINTS_DETAILS",
                param: parameters,
                commandType: CommandType.StoredProcedure));
        }
        public Task InsertPointsRedeem(DbInsertPointsRedeemModel request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Contact_Id", request.ContactId);
            parameters.Add("@Points", request.Points);
            parameters.Add("@Company_ID", request.CompanyId);
            parameters.Add("@Branch_ID", request.BranchId);
            parameters.Add("@Login_user_ID", request.LoginUserId);
            parameters.Add("@Notes", request.Notes);
            return ExecuteAsync(conn => conn.QueryAsync(
                sql: "dbo.INSERT_POINTS_REDEEM_DETAILS",
                param: parameters,
                commandType: CommandType.StoredProcedure));
        }
        public Task<DbPointsBalanceModel> GetPointsBalance(int contactId, long? companyId, long? branchId, long? loginUserId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Contact_Id", contactId);
            parameters.Add("@Company_ID", companyId);
            parameters.Add("@Branch_ID", branchId);
            parameters.Add("@Login_user_ID", loginUserId);
            return ExecuteAsync(conn => conn.QueryFirstOrDefaultAsync<DbPointsBalanceModel>(
                sql: "dbo.GET_POINTS_BALANCE",
                param: parameters,
                commandType: CommandType.StoredProcedure));
        }
        public Task<IEnumerable<DbCountryModel>> GetCountryList()
        {
            var parameters = new DynamicParameters();
            return ExecuteAsync(conn => conn.QueryAsync<DbCountryModel>(
                sql: "dbo.GetCountryList",
                param: parameters,
                commandType: CommandType.StoredProcedure));
        }
        public Task<IEnumerable<DbStateModel>> GetStatesList(int countryId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@COUNTRY_ID", countryId);
            return ExecuteAsync(conn => conn.QueryAsync<DbStateModel>(
                sql: "dbo.GetStateList",
                param: parameters,
                commandType: CommandType.StoredProcedure));
        }
        public Task<IEnumerable<DbOfferDetailsAdvanceSearchModel>> OfferAdvanceSearch(DbOfferDetailsAdvanceSearchRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Search_Text", request.SearchText);
            parameters.Add("@Company_ID", request.CompanyId);
            parameters.Add("@Branch_ID", request.BranchId);
            parameters.Add("@start", request.Start);
            parameters.Add("@end", request.End);
            parameters.Add("@Login_id", request.LoginId);

            return ExecuteAsync(conn => conn.QueryAsync<DbOfferDetailsAdvanceSearchModel>(
                sql: "dbo.GET_OfferDetails_AdvanceSearch",
                param: parameters,
                commandType: CommandType.StoredProcedure));
        }
        public Task<IEnumerable<DbCustomertModel>> GetCustomer(DbGetCustomerRequestModel request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@contact_id", request.ContactId);
            parameters.Add("@Company_ID", request.CompanyId);
            parameters.Add("@Branch_ID", request.BranchId);
            parameters.Add("@Login_id", request.LoginId);

            return ExecuteAsync(conn => conn.QueryAsync<DbCustomertModel>(
                sql: "dbo.CUSTOMER_DETAILS_GET",
                param: parameters,
                commandType: CommandType.StoredProcedure));
        }
        public Task<DbPointEnableModel> GetPointEnable(DbGetPointEnableRequestModel request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Contact_Id", request.ContactId);
            parameters.Add("@Company_ID", request.CompanyId);
            parameters.Add("@Branch_ID", request.BranchId);
            parameters.Add("@Login_user_ID", request.LoginId);

            return ExecuteAsync(conn => conn.QueryFirstAsync<DbPointEnableModel>(
                sql: "dbo.GET_POINTS_ENABLE",
                param: parameters,
                commandType: CommandType.StoredProcedure));
        }

        public Task SaveDocumentUrl(int id, string path)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@ID", id);
            parameters.Add("@LogoPath", path);
            
            return ExecuteAsync(conn => conn.QueryAsync(
                sql: "dbo.UPDATE_CUSTOMER_PIC",
                param: parameters,
                commandType: CommandType.StoredProcedure));
        }
        
    }
}
