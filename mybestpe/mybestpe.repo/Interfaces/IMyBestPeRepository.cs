﻿using mybestpe.baserepo.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace mybestpe.repo.Interfaces
{
    public interface IMyBestPeRepository
    {
        Task<DataSet> Bolt(string spName, Hashtable hashtable);
        Task<IEnumerable<DbVendorListingPositionModel>> VendorListingPosition(long? id, long companyId, long branchId, long? loginId, int? start, int? end);
        Task<IEnumerable<DbOfferDetailsModel>> GetOfferDetails(long? id, long companyId, long branchId, long? vendorId = null, long? categoryId = null, long? loginId = null,int? start = null, int? end = null);
        Task<IEnumerable<DbCategoryModel>> GetCategory(long? id,bool? isActive, long companyId, long branchId, long? loginId);
        Task<IEnumerable<DbOfferSlideModel>> GetOfferSlideDetails(long? offerDetailsId, long companyId, long branchId, long? loginId);
        Task<IEnumerable<DbOfferDetailsImgModel>> GetOfferDetailsImg(long? id, long companyId, long branchId, long? loginId);
        Task<DbChangePasswordModel> ChangePassword(DbChangePasswordRequestModel request);
        Task<DbForgetPasswordModel> ForgetPassword(string email);
        Task<DbUserRegisterModel> UserRegistration(DbRegisterRequestModel request);
        Task<int> UpdateCustomerDetails(DbUpdateCustomerDetailsModel request);
        Task<DbUserLoginModel> UserLogin(string email, string password);
        Task InsertOfferVisitDetails(DbInsertOfferVisitDetailsModel request);
        Task InsertPointsRedeem(DbInsertPointsRedeemModel request);
        Task InsertPointsDetails(DbInsertPointsDetailsModel request);
        Task<DbPointsBalanceModel> GetPointsBalance(int contactId, long? companyId, long?  branchId, long?  loginUserId);
        Task<IEnumerable<DbCountryModel>> GetCountryList();
        Task<IEnumerable<DbStateModel>> GetStatesList(int countryId);
        Task<IEnumerable<DbOfferDetailsAdvanceSearchModel>> OfferAdvanceSearch(DbOfferDetailsAdvanceSearchRequest request);
        Task<IEnumerable<DbCustomertModel>> GetCustomer(DbGetCustomerRequestModel request);
        Task<DbPointEnableModel> GetPointEnable(DbGetPointEnableRequestModel request);
        Task SaveDocumentUrl(int id, string path);
    }
}
