﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mybestpe.api.Auth
{
    public class JwtSettings
    {
        public string UserAttribute { get; set; }
        public string TokenKey { get; set; }
        public string TokenPublicKey { get; set; }
        public string TokenPublicKeyId { get; set; }
        public string TokenIssuer { get; set; }
    }
}
