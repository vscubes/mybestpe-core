﻿using Autofac;
using Microsoft.Extensions.Configuration;
using mybestpe.api.Service;
using mybestpe.repo.Implementations;
using mybestpe.repo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mybestpe.api
{
    public class AutofacModule : Module
    {
        private readonly string _dbConnectionString;        
        private readonly IConfigurationRoot _configurationRoot;
        public AutofacModule(string dbConnectionString,
            IConfigurationRoot configurationRoot)
        {
            _dbConnectionString = dbConnectionString;            
            _configurationRoot = configurationRoot;
        }
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.Register(n => new MyBestPeRepository(_dbConnectionString))
               .As<IMyBestPeRepository>()
               .SingleInstance();
            builder.RegisterType<SendMail>();
        }


    }
}
