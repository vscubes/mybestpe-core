﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using mybestpe.api.Model.Auth;
using mybestpe.repo.Interfaces;
using NLog;

namespace mybestpe.api.Controllers
{
    [Route("api")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private IMyBestPeRepository _repository;
        public AuthController(IMyBestPeRepository repository)
        {
            _repository = repository;
        }

        //[HttpPost("auth")]
        //public async Task<IActionResult> Login([FromBody]LoginModel login)
        //{
        //    Dictionary<string, string> dict = new Dictionary<string, string>();
        //    try
        //    {
        //        var validateResult = await _repository.ValidateUser(login.Username, login.Password);
        //        if (validateResult.is_error == 1)
        //        {
        //            return StatusCode(500, validateResult);
        //        }
        //        else
        //        {
        //            var loginResult = await _repository.Login(validateResult.id);

        //            var loginInfo = _loginInfoProvider.GetLoginInfo(login.Username, true, validateResult);
        //            var authToken = _jwtService.CreateJwtToken(login.Username);

        //            var result = MapLoginModel(loginInfo, authToken);

        //            return StatusCode(200, result);
        //        }
        //    }
        //    catch (SqlException e)
        //    {
        //        if (e.Number <= 50000)
        //        {
        //            _logger.Error(e, "Error while login");
        //        }
        //        _logger.Error(e, "Error while login");
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex, "Error while login");
        //    }

        //}
    }
}