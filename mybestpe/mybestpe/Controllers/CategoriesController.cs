﻿using Microsoft.AspNetCore.Mvc;
using mybestpe.api.Model.Categories;
using mybestpe.baserepo.Entities;
using mybestpe.repo.Interfaces;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace mybestpe.api.Controllers
{
    [Route("categories")]
    public class CategoriesController : Controller
    {
        private readonly IMyBestPeRepository _repository;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public CategoriesController(IMyBestPeRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("")]
        public async Task<IEnumerable<DbCategoryModel>> GetCategory([FromQuery]CategoryRequestModel request)
        {
            try
            {
                var dbResult = await _repository.GetCategory(request.Id, request.IsActive, request.CompanyId, request.BranchId, null);
                if (dbResult.Count() < 1)
                    return null;
                return dbResult
                    .Select(tx =>
                    {
                        return (tx);
                    })
                    .Select(c => new DbCategoryModel
                    {
                        Id = c.Id,
                        Name = c.Name,
                        Code = c.Code,
                        IsActive = c.IsActive,
                        ImageURL = c.SITE_URL + c.ImageURL
                    });
            }
            catch (SqlException e)
            {
                if (e.Number <= 50000)
                {
                    throw new Exception("Sql Custom Error : " + e.Message, e.InnerException);
                }
                throw new Exception("Sql Error : " + e.Message, e.InnerException);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while get categories");
                throw;
            }
        }
    }
}