﻿using Microsoft.AspNetCore.Mvc;
using mybestpe.api.Model.Categories;
using mybestpe.baserepo.Entities;
using mybestpe.repo.Interfaces;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace mybestpe.api.Controllers
{
    [Route("country")]
    public class CountryController : Controller
    {
        private readonly IMyBestPeRepository _repository;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public CountryController(IMyBestPeRepository repository)
        {
            _repository = repository;
        }
        [HttpGet("")]
        public async Task<IEnumerable<DbCountryModel>> GetCountryList()
        {
            try
            {
                var countryList = await _repository.GetCountryList();
                return countryList;
            }
            catch (SqlException e)
            {
                if (e.Number <= 50000)
                {
                    throw new Exception("Sql Custom Error : " + e.Message, e.InnerException);
                }
                throw new Exception("Sql Error : " + e.Message, e.InnerException);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while get country list");
                throw;
            }
        }

        [HttpGet("states")]
        public async Task<IEnumerable<DbStateModel>> GetStatesList([FromQuery]int countryId)
        {
            try
            {
                var statesList = await _repository.GetStatesList(countryId);
                return statesList;
            }
            catch (SqlException e)
            {
                if (e.Number <= 50000)
                {
                    throw new Exception("Sql Custom Error : " + e.Message, e.InnerException);
                }
                throw new Exception("Sql Error : " + e.Message, e.InnerException);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while get country list");
                throw;
            }
        }
    }
}