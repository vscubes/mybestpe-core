﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using mybestpe.api.Model.Documents;
using mybestpe.repo.Interfaces;
using NLog;

namespace mybestpe.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentsController : ControllerBase
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly IMyBestPeRepository _repository;
        public DocumentsController(IMyBestPeRepository repository)
        {
            _repository = repository;
        }

        [HttpPost]
        [Route("file_upload_base64")]
        public async Task<IActionResult> FileUploadBase64([FromBody] FileUploadBase64Model base64String)
        {
            try
            {
                if (!String.IsNullOrEmpty(base64String.Base64String))
                {
                    string extension = GetFileExtension(base64String.Base64String.Substring(base64String.Base64String.IndexOf(",") + 1));
                    string fileName = Guid.NewGuid().ToString() + "." + extension;
                    byte[] bytes = Convert.FromBase64String(base64String.Base64String.Substring(base64String.Base64String.IndexOf(",") + 1));

                    //when we will migrate to aws s3 than we need to replace below code with s3 upload code
                    
                    string checkPath = Path.GetFullPath(Path.Combine(@"C:\inetpub\wwwroot\mybestpeapp.vscubes.com\ContactLogo"));
                    _logger.Info("checkPath  :  " + checkPath);

                    var filePath = Path.Combine(checkPath, fileName);
                    _logger.Info("FilePath  :  " + filePath);
                    if (!Directory.Exists(checkPath))
                        Directory.CreateDirectory(checkPath);

                    System.IO.File.WriteAllBytes(filePath, bytes);

                    var serverImageURL = "https://mybestpe.vscubes.com/ContactLogo/" + fileName;
                    string[] url = new string[1] { serverImageURL };
                    return Ok(url);
                    //result = await _repository.SaveDocument(DocumentType, fileName, serverImageURL, UserId, VehicleId, BranchId);
                }
                return StatusCode(500, "some internal problem");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw;
            }

        }


        [HttpPost("SavePath")]
        public async Task SaveDocumentUrl([FromBody]SaveUrlModel request)
        {
            try
            {
                await _repository.SaveDocumentUrl(request.Id, request.Path);
            }
            catch (SqlException e)
            {
                if (e.Number <= 50000)
                {
                    throw new Exception("Sql Custom Error : " + e.Message, e.InnerException);
                }
                throw new Exception("Sql Error : " + e.Message, e.InnerException);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while update_customer");
                throw;
            }
        }
        private int GenerateRandomNo()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }

        private string GetFileExtension(string base64String)
        {
            var data = base64String.Substring(0, 5);
            switch (data.ToUpper())
            {
                case "IVBOR":
                    return "png";
                case "/9J/4":
                    return "jpg";
                case "AAAAF":
                    return "mp4";
                case "JVBER":
                    return "pdf";
                case "AAABA":
                    return "ico";
                case "UMFYI":
                    return "rar";
                case "E1XYD":
                    return "rtf";
                case "U1PKC":
                    return "txt";
                case "MQOWM":
                case "77U/M":
                    return "srt";
                default:
                    return "png";
            }
        }

    }
}