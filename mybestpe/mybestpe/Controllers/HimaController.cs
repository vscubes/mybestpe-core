﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using mybestpe.api.Model.Hima;
using mybestpe.repo.Interfaces;
using NLog;

namespace mybestpe.api.Controllers
{
    [Route("hima")]
    [ApiController]
    public class HimaController : ControllerBase
    {
        private readonly IMyBestPeRepository _repository;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly TimeSpan _cacheExpiry;

        public HimaController(IMyBestPeRepository repository)
        {
            _repository = repository;
        }

        [HttpPost("")]
        public async Task<IActionResult> Bolt(HimaRequestModel request)
        {
            _logger.Info("BOLT API CONTROLLER METHOD HIT.");
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            Dictionary<string, object> dictionary1 = new Dictionary<string, object>();
            Hashtable ht = new Hashtable();
            Hashtable ht_Check = new Hashtable();
            DataSet ds = new DataSet();
            string ErrorState, ErrorMessage;

            try
            {
                ht.Clear();
                if (request.ActionMode != "" && request.ActionMode != null)
                {
                    ht.Add("action", request.ActionMode);
                }
                else
                {
                    ht.Add("action", "run");
                }

                ht.Add("objectName", request.objectName.Trim());
                ht.Add("data", request.data.Trim());
                if (request.pkValue != "" && request.pkValue != null)
                {
                    ht.Add("pkValue", request.pkValue);
                }
                ds = await _repository.Bolt("[BOLT_API]", ht);
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = new DataTable();

                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        dt.Clear();
                        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                        if (ds.Tables[i].Rows.Count > 0)
                        {
                            dt = ds.Tables[i];

                            DataColumnCollection columns = dt.Columns;
                            if (columns.Contains("ErrorState") && columns.Contains("ErrorMessage"))
                            {
                                ErrorState = ds.Tables[i].Rows[0]["ErrorState"].ToString();
                                ErrorMessage = ds.Tables[i].Rows[0]["ErrorMessage"].ToString();

                                if (ErrorState == "1")
                                {
                                    ht.Clear();
                                    ht.Add("@Type", "API");
                                    ht.Add("@APIKey", "PUBLIC_BOLT_API");
                                    ht.Add("@Module", request.objectName.Trim());
                                    ht.Add("@Error", ErrorMessage.ToString());
                                    ds = await _repository.Bolt("[API_INSERT_ErrorLog]", ht);
                                }
                            }

                            Dictionary<string, object> row;
                            foreach (DataRow dr in dt.Rows)
                            {
                                row = new Dictionary<string, object>();
                                foreach (DataColumn col in dt.Columns)
                                {
                                    row.Add(col.ColumnName, dr[col] == DBNull.Value ? null : dr[col]);
                                }
                                rows.Add(row);
                            }
                        }
                        dictionary1.Add(i.ToString(), rows);
                    }
                    dictionary.Add("data", dictionary1);
                    dictionary.Add("error", false);
                    dictionary.Add("message", "success");
                    //HttpContext.Current.Response.AppendHeader("ResponseHeader", "200");
                }
                else
                {
                    dictionary.Add("data", dictionary1);
                    dictionary.Add("error", false);
                    dictionary.Add("message", "success");
                    return StatusCode(200, dictionary);
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while bolt call");
                throw;
            }
            return StatusCode(200, dictionary);
        }
    }
}