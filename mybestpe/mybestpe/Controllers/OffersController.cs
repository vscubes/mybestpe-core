﻿using Microsoft.AspNetCore.Mvc;
using mybestpe.api.Model.Offers;
using mybestpe.api.Model.Vendor;
using mybestpe.baserepo.Entities;
using mybestpe.repo.Interfaces;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace mybestpe.api.Controllers
{
    [Route("offers")]
    public class OffersController : Controller
    {
        private readonly IMyBestPeRepository _repository;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public OffersController(IMyBestPeRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("offer_list")]
        public async Task<IEnumerable<DbOfferDetailsModel>> GetOfferDetails([FromQuery]OfferDetailsRequestModel request)
        {
            try
            {
                var dbResult = await _repository.GetOfferDetails(request.Id, request.CompanyId, request.BranchId, request.VendorId, request.CategoryId, start:request.Start, end:request.End);
                if (dbResult.Count() < 1)
                    return null;
                return dbResult
                    .Select(tx =>
                    {
                        return (tx);
                    })
                    .Select(c => new DbOfferDetailsModel
                    {
                        Id = c.Id,
                        OfferDetails = c.OfferDetails,
                        Logo = c.SITE_URL + c.Logo,
                        AboutDetails = c.AboutDetails,
                        CoupenCode = c.CoupenCode,
                        RedemptionProcessLink = c.RedemptionProcessLink,
                        RedemptionProcessDetails = c.RedemptionProcessDetails,
                        TermsandConditions = c.TermsandConditions,
                        ValidStartDate = c.ValidStartDate,
                        ValidEndDate = c.ValidEndDate,
                        CustomerCareNumber = c.CustomerCareNumber,
                        CustomerCareMail = c.CustomerCareMail,
                        CommissionSlab = c.CommissionSlab,
                        CategoryId = c.CategoryId,
                        IsActive = c.IsActive,
                        CategoryName = c.CategoryName,
                        VendorId = c.VendorId,
                        DiscountType = c.DiscountType,
                        DiscountAmount = c.DiscountAmount,
                        DiscountUpto = c.DiscountUpto,
                    });
            }
            catch (SqlException e)
            {
                if (e.Number <= 50000)
                {
                    throw new Exception("Sql Custom Error : " + e.Message, e.InnerException);
                }
                throw new Exception("Sql Error : " + e.Message, e.InnerException);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while get offers");
                throw;
            }
        }

        [HttpGet("top_offer_list")]
        public async Task<IEnumerable<DbOfferDetailsModel>> GetTopOfferList([FromQuery]TopOfferRequestModel request)
        {
            try
            {
                var dbResult = await _repository.GetOfferDetails(null,request.CompanyId, request.BranchId, start:request.Start, end:request.End);
                if (dbResult.Count() < 1)
                    return null;
                return dbResult
                    .Select(tx =>
                    {
                        return (tx);
                    })
                    .Select(c => new DbOfferDetailsModel
                    {
                        Id = c.Id,
                        OfferDetails = c.OfferDetails,
                        Logo = c.SITE_URL + c.Logo,
                        AboutDetails = c.AboutDetails,
                        CoupenCode = c.CoupenCode,
                        RedemptionProcessLink = c.RedemptionProcessLink,
                        RedemptionProcessDetails = c.RedemptionProcessDetails,
                        TermsandConditions = c.TermsandConditions,
                        ValidStartDate = c.ValidStartDate,
                        ValidEndDate = c.ValidEndDate,
                        CustomerCareNumber = c.CustomerCareNumber,
                        CustomerCareMail = c.CustomerCareMail,
                        CommissionSlab = c.CommissionSlab,
                        CategoryId = c.CategoryId,
                        IsActive = c.IsActive,
                        CategoryName = c.CategoryName,
                        VendorId = c.VendorId,
                        DiscountType = c.DiscountType,
                        DiscountAmount = c.DiscountAmount,
                        DiscountUpto = c.DiscountUpto,
                    });
            }
            catch (SqlException e)
            {
                if (e.Number <= 50000)
                {
                    throw new Exception("Sql Custom Error : " + e.Message, e.InnerException);
                }
                throw new Exception("Sql Error : " + e.Message, e.InnerException);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while get top_offer_list");
                throw;
            }
        }

        [HttpGet("slide")]
        public async Task<IEnumerable<DbOfferSlideModel>> GetOfferSlideDetails([FromQuery]OfferDetailsSlideShowRequestModel request)
        {
            try
            {
                var dbResult = await _repository.GetOfferSlideDetails(request.OfferDetailsId, request.CompanyId, request.BranchId, null);

                if (dbResult.Count() < 1)
                    return null;
                return dbResult
                    .Select(tx =>
                    {
                        return (tx);
                    })
                    .Select(c => new DbOfferSlideModel
                    {
                        Id = c.Id,
                        OfferDetailsId = c.OfferDetailsId,
                        Link = c.Link,
                        Price = c.Price,
                        ValidStartDate = c.ValidStartDate,
                        ValidEndDate = c.ValidEndDate,
                        IsActive = c.IsActive,
                        OfferDetails = c.OfferDetails,
                        Logo = c.SITE_URL + c.Logo
                    });
            }
            catch (SqlException e)
            {
                if (e.Number <= 50000)
                {
                    throw new Exception("Sql Custom Error : " + e.Message, e.InnerException);
                }
                throw new Exception("Sql Error : " + e.Message, e.InnerException);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while get offers slide");
                throw;
            }
        }

        [HttpGet("offer_details")]
        public async Task<IEnumerable<OfferDetailsWithMediaModel>> GetOfferDtailsWithMedia([FromQuery]OfferDtailsWithMediaRequestModel request)
        {
            try
            {
                var offerDetails = await _repository.GetOfferDetails(request.Id, request.CompanyId, request.BranchId);
                if (offerDetails.Count() < 1)
                    return null;

                var DbofferDetailsImg = await _repository.GetOfferDetailsImg(request.Id, request.CompanyId, request.BranchId, null);
                if (DbofferDetailsImg.Count() < 1)
                    DbofferDetailsImg = null;
                var offerDetailsImg =  DbofferDetailsImg
                    .Select(tx =>
                    {
                        return (tx);
                    })
                    .Select(c => new DbOfferDetailsImgModel
                    {
                        Id = c.Id,
                        OfferDetailsId = c.OfferDetailsId,
                        Img = c.SITE_URL + c.Img,
                        IsDefault = c.IsDefault,
                        IsActive = c.IsActive
                    });

                return offerDetails
                    .Select(tx =>
                    {
                        return (tx);
                    })
                    .Select(c => new OfferDetailsWithMediaModel
                    {
                        Id = c.Id,
                        OfferDetails = c.OfferDetails,
                        Logo = c.SITE_URL + c.Logo,
                        AboutDetails = c.AboutDetails,
                        CoupenCode = c.CoupenCode,
                        RedemptionProcessLink = c.RedemptionProcessLink,
                        RedemptionProcessDetails = c.RedemptionProcessDetails,
                        TermsandConditions = c.TermsandConditions,
                        ValidStartDate = c.ValidStartDate,
                        ValidEndDate = c.ValidEndDate,
                        CustomerCareNumber = c.CustomerCareNumber,
                        CustomerCareMail = c.CustomerCareMail,
                        CommissionSlab = c.CommissionSlab,
                        CategoryId = c.CategoryId,
                        IsActive = c.IsActive,
                        CategoryName = c.CategoryName,
                        VendorId = c.VendorId,
                        DiscountType = c.DiscountType,
                        DiscountAmount = c.DiscountAmount,
                        DiscountUpto = c.DiscountUpto,
                        OfferDetailsImg = DbofferDetailsImg == null ? null : offerDetailsImg.Where(a => a.OfferDetailsId == c.Id)
                    });
            }
            catch (SqlException e)
            {
                if (e.Number <= 50000)
                {
                    throw new Exception("Sql Custom Error : " + e.Message, e.InnerException);
                }
                throw new Exception("Sql Error : " + e.Message, e.InnerException);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while get offer_dtails_with_media");
                throw;
            }
        }

        [HttpPost("offer_visit_details")]
        public async Task InsertOfferVisitDetails([FromBody]DbInsertOfferVisitDetailsModel request)
        {
            try
            {
                await _repository.InsertOfferVisitDetails(request);
            }
            catch (SqlException e)
            {
                if (e.Number <= 50000)
                {
                    throw new Exception("Sql Custom Error : " + e.Message, e.InnerException);
                }
                throw new Exception("Sql Error : " + e.Message, e.InnerException);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while insert offer_visit_details");
                throw;
            }
        }

        [HttpGet("advance_search")]
        public async Task<IEnumerable<DbOfferDetailsAdvanceSearchModel>> OfferAdvanceSearch([FromQuery]DbOfferDetailsAdvanceSearchRequest request)
        {
            try
            {
               var result = await _repository.OfferAdvanceSearch(request);
                if (result == null)
                    return null;
                if (result.Count() < 1)
                    return null;
                return result
                    .Select(tx =>
                    {
                        return (tx);
                    })
                    .Select(c => new DbOfferDetailsAdvanceSearchModel
                    {
                        id = c.id,
                        OfferDetails = c.OfferDetails,
                        AboutDetails = c.AboutDetails,
                        CoupenCode = c.CoupenCode,
                        RedemptionProcessLink = c.RedemptionProcessLink,
                        RedemptionProcessDetails = c.RedemptionProcessDetails,
                        TermsandConditions = c.TermsandConditions,
                        ValidStartDate = c.ValidStartDate,
                        ValidEndDate = c.ValidEndDate,
                        CustomerCareNumber = c.CustomerCareNumber,
                        CustomerCareMail = c.CustomerCareMail,
                        CommissionSlab = c.CommissionSlab,
                        CategoryId = c.CategoryId,
                        IsActive = c.IsActive,
                        CategoryName = c.CategoryName,
                        VendorId = c.VendorId,
                        DiscountType = c.DiscountType,
                        DiscountAmount = c.DiscountAmount,
                        DiscountUpto = c.DiscountUpto,
                        Logo = c.SiteUrl + c.Logo,
                        SiteUrl = c.SiteUrl,
                        VendorName = c.VendorName
                    });
            }
            catch (SqlException e)
            {
                if (e.Number <= 50000)
                {
                    throw new Exception("Sql Custom Error : " + e.Message, e.InnerException);
                }
                throw new Exception("Sql Error : " + e.Message, e.InnerException);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while get advance_search");
                throw;
            }
        }
    }
}