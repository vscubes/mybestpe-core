﻿using Microsoft.AspNetCore.Mvc;
using mybestpe.api.Model.Offers;
using mybestpe.api.Model.Points;
using mybestpe.api.Model.Vendor;
using mybestpe.baserepo.Entities;
using mybestpe.repo.Interfaces;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace mybestpe.api.Controllers
{
    [Route("points")]
    public class PointsController : Controller
    {
        private readonly IMyBestPeRepository _repository;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public PointsController(IMyBestPeRepository repository)
        {
            _repository = repository;
        }

        [HttpPost("points_details")]
        public async Task InsertPointsDetails([FromBody]DbInsertPointsDetailsModel request)
        {
            try
            {
                await _repository.InsertPointsDetails(request);
            }
            catch (SqlException e)
            {
                if (e.Number <= 50000)
                {
                    throw new Exception("Sql Custom Error : " + e.Message, e.InnerException);
                }
                throw new Exception("Sql Error : " + e.Message, e.InnerException);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while insert points_details");
                throw;
            }
        }

        [HttpGet("points_balance")]
        public async Task<DbPointsBalanceModel> GetPointsBalance([FromQuery]PointsBalanceModel request)
        {
            try
            {
                var pointsBalance = await _repository.GetPointsBalance(request.ContactId, request.CompanyId, request.BranchId, request.LoginUserId);
                return pointsBalance;
            }
            catch (SqlException e)
            {
                if (e.Number <= 50000)
                {
                    throw new Exception("Sql Custom Error : " + e.Message, e.InnerException);
                }
                throw new Exception("Sql Error : " + e.Message, e.InnerException);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while get points_balance");
                throw;
            }
        }

        [HttpPost("points_redeem")]
        public async Task PointsRedeem([FromBody]DbInsertPointsRedeemModel request)
        {
            try
            {
                await _repository.InsertPointsRedeem(request);
            }
            catch (SqlException e)
            {
                if (e.Number <= 50000)
                {
                    throw new Exception("Sql Custom Error : " + e.Message, e.InnerException);
                }
                throw new Exception("Sql Error : " + e.Message, e.InnerException);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while insert points_redeem");
                throw;
            }
        }

        [HttpGet("enable")]
        public async Task<DbPointEnableModel> GetPointEnable([FromQuery]DbGetPointEnableRequestModel request)
        {
            try
            {
                var result = await _repository.GetPointEnable(request);
                return result;
            }
            catch (SqlException e)
            {
                if (e.Number <= 50000)
                {
                    throw new Exception("Sql Custom Error : " + e.Message, e.InnerException);
                }
                throw new Exception("Sql Error : " + e.Message, e.InnerException);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while get enable");
                throw;
            }
        }
    }
}