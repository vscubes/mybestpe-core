﻿using Microsoft.AspNetCore.Mvc;
using mybestpe.api.Model;
using mybestpe.api.Model.Categories;
using mybestpe.api.Service;
using mybestpe.baserepo.Entities;
using mybestpe.repo.Interfaces;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace mybestpe.api.Controllers
{
    [Route("users")]
    public class UsersController : Controller
    {
        private readonly IMyBestPeRepository _repository;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private SendMail _sendMail;

        public UsersController(IMyBestPeRepository repository, SendMail sendMail)
        {
            _repository = repository;
            _sendMail = sendMail;
        }

        [HttpPost("change_password")]
        public async Task<DbChangePasswordModel> ChangePassword([FromBody]DbChangePasswordRequestModel request)
        {
            try
            {
                var result = await _repository.ChangePassword(request);
                return result;
            }
            catch (SqlException e)
            {
                if (e.Number <= 50000)
                {
                    throw new Exception("Sql Custom Error : " + e.Message,e.InnerException);
                }
                throw new Exception("Sql Error : " + e.Message, e.InnerException);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while reset_password");
                throw;
            }
        }

        [HttpPost("forget_password")]
        public async Task<DbForgetPasswordModel> ForgetPassword([FromQuery(Name = "Email")][Required] string Email)
        {
            try
            {
                var result = await _repository.ForgetPassword(Email);
                if (!string.IsNullOrEmpty(result.Password))
                {
                    _sendMail.SendVerifyCode(result.Password, Email, EMailMessage.SendTempPasswordBody, EMailMessage.SendTempPasswordSubject);
                }
                return result;
            }
            catch (SqlException e)
            {
                if (e.Number <= 50000)
                {
                    throw new Exception("Sql Custom Error : " + e.Message, e.InnerException);
                }
                throw new Exception("Sql Error : " + e.Message, e.InnerException);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while forget_password");
                throw;
            }
        }

        [HttpPost("registration")]
        public async Task<DbUserRegisterModel> UserRegistration([FromBody]DbRegisterRequestModel request)
        {
            try
            {
                var result = await _repository.UserRegistration(request);
                return result;
            }
            catch (SqlException e)
            {
                if (e.Number <= 50000)
                {
                    throw new Exception("Sql Custom Error : " + e.Message, e.InnerException);
                }
                throw new Exception("Sql Error : " + e.Message, e.InnerException);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while registration");
                throw;
            }
        }

        [HttpPost("update_customer")]
        public async Task<int> UpdateCustomerDetails([FromBody]DbUpdateCustomerDetailsModel request)
        {
            try
            {
                var result = await _repository.UpdateCustomerDetails(request);
                return result;
            }
            catch (SqlException e)
            {
                if (e.Number <= 50000)
                {
                    throw new Exception("Sql Custom Error : " + e.Message, e.InnerException);
                }
                throw new Exception("Sql Error : " + e.Message, e.InnerException);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while update_customer");
                throw;
            }
        }


        [HttpGet("customer")]
        public async Task<IEnumerable<DbCustomertModel>> GetCustomer([FromQuery]DbGetCustomerRequestModel request)
        {
            try
            {
                var result = await _repository.GetCustomer(request);
                return result;
            }
            catch (SqlException e)
            {
                if (e.Number <= 50000)
                {
                    throw new Exception("Sql Custom Error : " + e.Message, e.InnerException);
                }
                throw new Exception("Sql Error : " + e.Message, e.InnerException);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while get customer");
                throw;
            }
        }

        [HttpPost("login")]
        public async Task<DbUserLoginModel> UserLogin([FromQuery(Name = "Email")][Required] string Email, [FromQuery(Name = "Password")][Required] string Password)
        {
            try
            {
                var result = await _repository.UserLogin(Email, Password);
                return result;
            }
            catch (SqlException e)
            {
                if (e.Number <= 50000)
                {
                    throw new Exception("Sql Custom Error : " + e.Message, e.InnerException);
                }
                throw new Exception("Sql Error : " + e.Message, e.InnerException);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while login");
                throw;
            }
        }
    }
}