﻿using Microsoft.AspNetCore.Mvc;
using mybestpe.api.Model.Vendor;
using mybestpe.baserepo.Entities;
using mybestpe.repo.Interfaces;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace mybestpe.api.Controllers
{
    [Route("vendor")]
    public class VendorController : Controller
    {
        private readonly IMyBestPeRepository _repository;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public VendorController(IMyBestPeRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("vendor_list")]
        public async Task<IEnumerable<DbVendorListingPositionModel>> VendorListingPosition([FromQuery]VendorListingPositionRequestModel request)
        {
            try
            {
                var dbResult = await _repository.VendorListingPosition(request.Id, request.CompanyId, request.BranchId, null, request.Start, request.End);
                if (dbResult.Count() < 1)
                    return null;
                return dbResult
                    .Select(tx =>
                    {
                        return (tx);
                    })
                    .Select(c => new DbVendorListingPositionModel
                    {
                        Id = c.Id,
                        VendorId = c.VendorId,
                        Link = c.Link,
                        Price = c.Price,
                        ValidStartDate = c.ValidStartDate,
                        ValidEndDate = c.ValidEndDate,
                        IsActive = c.IsActive,
                        VendorName = c.VendorName,
                        Position = c.Position,
                        ImageURL = c.SITE_URL + c.ImageURL
                    });
            }
            catch (SqlException e)
            {
                if (e.Number <= 50000)
                {
                    throw new Exception("Sql Custom Error : " + e.Message, e.InnerException);
                }
                throw new Exception("Sql Error : " + e.Message, e.InnerException);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Error while get listing_position");
                throw;
            }
        }
    }
}