﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mybestpe.api.Model.Categories
{
    public class CategoryRequestModel
    {
        public long? Id { get; set; }
        public bool? IsActive { get; set; }
        public long CompanyId { get; set; }
        public long BranchId { get; set; }
    }
}
