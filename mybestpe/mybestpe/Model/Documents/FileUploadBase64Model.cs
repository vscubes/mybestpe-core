﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mybestpe.api.Model.Documents
{
    public class FileUploadBase64Model
    {
        public string Base64String { get; set; }
    }
}
