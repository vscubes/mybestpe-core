﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mybestpe.api.Model
{
    public static class EMailMessage
    {
        public static string SendVerifyCodeSubject = "Verify code";
        public static string SendVerifyCodeBody = "Your verify code is : ";
        public static string SendTempPasswordSubject = "MybestPe Temporary Password";
        public static string SendTempPasswordBody = "Your Temp. password is : ";
        public static string SendVerifyCodeAndTempPasswordSubject = "Temp. password and verify code";
        public static string SendVerifyCodeAndTempPasswordBody = "Your Temp. password and verify code is : ";
    }

}
