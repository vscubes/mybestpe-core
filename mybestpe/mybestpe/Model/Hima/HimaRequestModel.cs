﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mybestpe.api.Model.Hima
{
    public class HimaRequestModel
    {
        public string objectName { get; set; }
        public string data { get; set; }
        public string ActionMode { get; set; }
        public string pkValue { get; set; }
        public string personID { get; set; }
        public string companyID { get; set; }
        public string ipAddress { get; set; }
        public string userAgent { get; set; }
        public string sessionID { get; set; }
    }
}
