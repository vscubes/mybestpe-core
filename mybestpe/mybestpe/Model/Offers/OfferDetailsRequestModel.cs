﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mybestpe.api.Model.Offers
{
    public class OfferDetailsRequestModel
    {
        public long? Id { get; set; }
        public long CompanyId { get; set; }
        public long BranchId { get; set; }
        public long? VendorId { get; set; }
        public long? CategoryId { get; set; }
        public int? Start { get; set; }
        public int? End { get; set; }
    }
}
