﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mybestpe.api.Model.Offers
{
    public class OfferDetailsSlideShowRequestModel
    {
        public long? OfferDetailsId { get; set; }
        public long CompanyId { get; set; }
        public long BranchId { get; set; }
    }
}
