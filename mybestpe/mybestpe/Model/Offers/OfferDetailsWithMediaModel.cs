﻿using mybestpe.baserepo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mybestpe.api.Model.Offers
{
    public class OfferDetailsWithMediaModel
    {
        public long Id { get; set; }
        public string OfferDetails { get; set; }
        public string Logo { get; set; }
        public string AboutDetails { get; set; }
        public string CoupenCode { get; set; }
        public string RedemptionProcessLink { get; set; }
        public string RedemptionProcessDetails { get; set; }
        public string TermsandConditions { get; set; }
        public DateTime? ValidStartDate { get; set; }
        public DateTime? ValidEndDate { get; set; }
        public string CustomerCareNumber { get; set; }
        public string CustomerCareMail { get; set; }
        public string CommissionSlab { get; set; }
        public int? CategoryId { get; set; }
        public bool? IsActive { get; set; }
        public string CategoryName { get; set; }
        public int? VendorId { get; set; }
        public int? DiscountType { get; set; }
        public decimal? DiscountAmount { get; set; }
        public decimal? DiscountUpto { get; set; }
        public IEnumerable<DbOfferDetailsImgModel> OfferDetailsImg { get; set; }
    }
}
