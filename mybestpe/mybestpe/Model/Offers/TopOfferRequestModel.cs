﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mybestpe.api.Model.Offers
{
    public class TopOfferRequestModel
    {
        public long CompanyId { get; set; }
        public long BranchId { get; set; }
        public int? Start { get; set; }
        public int? End { get; set; }
    }
}
