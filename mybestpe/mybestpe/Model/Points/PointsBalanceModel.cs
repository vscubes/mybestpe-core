﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mybestpe.api.Model.Points
{
    public class PointsBalanceModel
    {
        public int ContactId { get; set; }
        public long? CompanyId { get; set; }
        public long? BranchId { get; set; }
        public long? LoginUserId { get; set; }
    }
}
