﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mybestpe.api.Model.Vendor
{
    public class VendorListingPositionRequestModel
    {
        public long? Id { get; set; }
        public long CompanyId { get; set; }
        public long BranchId { get; set; }
        public int? Start { get; set; }
        public int? End { get; set; }
    }
}
