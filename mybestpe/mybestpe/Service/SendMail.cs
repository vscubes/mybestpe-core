﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Mail;
using NLog;

namespace mybestpe.api.Service
{
    public class SendMail
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        public SendMail()
        {

        }
        public void SendVerifyCode(string code, string email, string message_body, string message_subject)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress("rk0908348@gmail.com");
                mail.To.Add(email);
                mail.Subject = message_subject;
                mail.Body = message_body + code;

                SmtpServer.Port = 587;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new System.Net.NetworkCredential("rk0908348@gmail.com", "rakesh#1");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "FATAL ERROR");
            }
        }
    }
}
